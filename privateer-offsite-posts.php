<?php
namespace Ninja\Privateer\Wp\Plugins\OffsitePosts;
/*
Plugin Name: Privateer Offsite Posts
Description: Set up post types to link off site to articles you have created elsewhere.
Version: 1.0.3
Author: Tony Jennings @ Privateer Web Solutions
Author URI: http://privateer.ninja/
*/
/*
Copyright 2015 Privateer Web Solutions (email : tony@privateer.ninja)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
*/

defined('\\ABSPATH') or die('Access Denied');


try {
	class Offsite_Posts_Exception extends \Exception {};

	$plugin_file_path = plugin_dir_path( __FILE__ );
	$plugin_dir_path = plugin_dir_url( __FILE__ );

	if ( false === include_once $plugin_file_path . 'include/class-offsite-posts-core.php' ) {
		throw new Offsite_Posts_Exception( 'Failed to load class Privateer Offsite Posts Core', 10001 );
	}

	if ( is_admin() ) {
		if ( false === include_once $plugin_file_path . 'include/class-offsite-posts-admin.php' ) {
			throw new Offsite_Posts_Exception( 'Failed to load class Privateer Offsite Posts Admin', 10001 );
		}
		new Admin( $plugin_file_path, $plugin_dir_path, 'pws_osp_', 'pws_osp_plugin_options' );
	} else {
		if ( false === include_once $plugin_file_path . 'include/class-offsite-posts-front-end.php' ) {
			throw new Offsite_Posts_Exception( 'Failed to load class Privateer Offsite Posts Front End', 10001 );
		}
		new Front_End( $plugin_file_path,  $plugin_dir_path, 'pws_osp_', 'pws_osp_plugin_options' );
	}

} catch ( \Exception $e ) {
	$die_on_error = false;
	if ( defined( '\\WP_DEBUG' ) && \WP_DEBUG ) {
		$die_on_error = true;
		if ( defined('\\WP_DEBUG_LOG' ) && \WP_DEBUG_LOG ) {
			if ( defined( '\\WP_DEBUG_DISPLAY' ) && ! \WP_DEBUG_DISPLAY ) {
				$die_on_error = false;
			}
		}
	}
	$error_out = apply_filters( 'pws_osp__die_on_exception', $die_on_error, $e );
	if ( $error_out ) {
		$code = $e->getCode();
		$message = $e->getMessage();
		$trace = print_r( $e->getTrace(), true );
		$line = $e->getLine();
		$file = $e->getFile();

		if ( current_user_can('activate_plugins') ) {
			$output = <<<HTML
<h2>Privateer Offsite Posts Plugin Error #{$code}</h2>
<h3>Location: {$file}, line {$line}</h3>
<p>{$message}</p>
<pre>{$trace}</pre>
HTML;

			wp_die( $output, "Privateer Offsite Posts Plugin Error #{$code}: {$message}" );
		}
	}
}
