# README #

This is a simple wordpress plugin designed to let one mix off site articles into their site. Originally, this was created so that I could write posts on steamit.com and have them shown in my blog and archive directories on my site, linking directly out to steamit.com articles that I have written.

### What is this repository for? ###

* A place to store the source code for this plugin

### How do I get set up? ###

* Grab the contents of this archive, stick them in a directory named privateer-offsite-posts, and upload that directory to your wordpress site in wp-content/plugins/ 
* Alternately, download the file privateer-offsite-posts.zip and upload that through your wordpress admin panel.
* Activate the plugin from your wordpress admin
* Go to Settings - Reading and choose which post types you want to be able to redirect to another site and, optionally, set a referrer (will add ?r=xxxxxx where xxxxxx is what you enter in this box) to any links
* Posts of the selected types will now have an offsite redirect url meta box where you can put links from other sites around the internet. When clicked in your blog or on an archive page, links to this post will take you to the entered url. Leave empty to have no redirect.

### Contribution guidelines ###

* To be determined

### Who do I talk to? ###

* Admin