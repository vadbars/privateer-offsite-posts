<?php
namespace Ninja\Privateer\Wp\Plugins\OffsitePosts;

class Offsite_Posts_Core {

	protected $_options_key = 'pws_osp_plugin_options';
	protected $_options = null;

	protected $_base_path = '';
	protected $_base_url = '';
	protected $_prefix = '';
	protected $_option_key = '';

	protected $_available_post_types = null;

	protected function __construct(
		$plugin_path = '',
		$plugin_url = '',
		$plugin_prefix = 'pws_osp_',
		$options_key = 'pws_osp_plugin_options'
	) {
		$this->_base_path = trailingslashit( $plugin_path );
		$this->_base_url = $plugin_url;
		$this->_prefix = $plugin_prefix;
		$this->_option_key = $options_key;

		$this->do_initialization();
	}

	# Common initialization for admin and front end
	protected function do_initialization() {

	}

	protected function get_option( $key, $default_value = null ) {
		$current = $this->get_options();
		return array_key_exists( $key, $current )? $current["{$key}"] : $default_value;
	}

	protected function get_options() {
		if ( is_null( $this->_options ) ) {
			$defaults = $this->get_default_options();
			$this->_options = get_option( $this->_options_key, $defaults );
		}
		return $this->_options;
	}

	protected function get_default_options() {
		return array(
			'use_post_types' => array(),
			'referrer' => ''
		);
	}

	protected function get_available_post_types() {
		if ( is_null( $this->_available_post_types ) ) {
			$available = array();
			$args = array(
				'public' => true
			);
			$public_post_types = get_post_types( $args, 'objects' );
			# echo '<p>Post Types:</p><pre>' . print_r($public_post_types, true) . '</pre>';
			if ( is_array( $public_post_types ) && 0 < count( $public_post_types ) ) {
				$require_editor = apply_filters( "{$this->_prefix}require_editor", true );
				foreach ( $public_post_types as $post_type_name => $pt ) {
					if ( ( ! $require_editor ) || post_type_supports( $post_type_name, 'editor' ) ) {
						if ( isset( $pt->labels ) && is_array( $pt->labels ) && array_key_exists( 'name', $pt->labels ) && ! empty( $pt->labels['name'] ) ) {
							$display_name = $pt->labels['name'];
						} else {
							if ( isset( $pt->label ) && ! empty( $pt->label ) ) {
								$display_name = $pt->label;
							} else {
								$display_name = $post_type_name;
							}
						}
						$available["{$post_type_name}"] = $display_name;
					} else if ( ! post_type_supports( $post_type_name, 'editor' ) ) {
						# echo "{$post_type_name} does not support editor";
					} else {
						# echo "<p>Editor required?" . (($require_editor)? 'yes' : 'no') . '</p>';
					}
				}
			}
			$this->_available_post_types = $available;
		}
		return $this->_available_post_types;
	}

	protected function handle_exception( \Exception $e, $die_on_error = false, $write_to_error_log = false ) {
		if ( defined('\\WP_DEBUG') && \WP_DEBUG ) {
			$die_on_error = true;
			if ( defined('\\WP_DEBUG_LOG') && \WP_DEBUG_LOG ) {
				$write_to_error_log = true;
				if ( defined('\\WP_DEBUG_DISPLAY') && ! \WP_DEBUG_DISPLAY ) {
					$die_on_error = false;
				}
			}
		}
		$error_out = apply_filters( "{$this->_prefix}die_on_exception", $die_on_error, $e );
		$email_admin = apply_filters( "{$this->_prefix}mail_admin_exception", false, $e);
		$code = $e->getCode();
		$message = $e->getMessage();
		$trace = print_r( $e->getTrace(), true );
		$line = $e->getLine();
		$file = $e->getFile();
		if ( $error_out || $email_admin ) {

			$output = <<<HTML
<h2>Privateer Offsite Posts Error #{$code}</h2>
<h3>Location: {$file}, line {$line}</h3>
<p>{$message}</p>
<pre>{$trace}</pre>
HTML;

			if ( $email_admin ) {
				$admin_email = apply_filters( $this->_prefix . 'mail_exceptions_to', get_option('admin_email', '') );
				if ( !empty($admin_email) ) {
					wp_mail( $admin_email, "Privateer Offsite Posts Error #{$code}", $output );
				}
			}

			if ( $error_out ) {
				wp_die( $output, "Privateer Offsite Posts Error #{$code}: {$message}" );
			}
		}
		if ( $write_to_error_log ) {
			error_log("Privateer Offsite Posts Error {$code} ({$file}:{$line}): {$message}:trace: {$trace}");
		}
	}
}