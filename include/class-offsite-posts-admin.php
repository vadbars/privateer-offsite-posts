<?php
namespace Ninja\Privateer\Wp\Plugins\OffsitePosts;

class Admin extends Offsite_Posts_Core {

	protected $_saved_meta_boxes = false;

	public function __construct(
		$plugin_path = '',
		$plugin_url = '',
		$plugin_prefix = 'pws_osp_',
		$options_key = 'pws_osp_plugin_options'
	) {
		parent::__construct( $plugin_path, $plugin_url, $plugin_prefix, $options_key );
	}

	protected function do_initialization() {
		parent::do_initialization();

		add_action('admin_init', array( $this, 'setup_plugin_settings' ) );

		$default_post_types = array();
		$allowed_types = $this->get_option('use_post_types', $default_post_types);
		if ( is_array( $allowed_types ) && 0 < count( $allowed_types ) ) {
			foreach ( $allowed_types as $post_type_name ) {
				add_action('add_meta_boxes_' . $post_type_name, array( $this, 'setup_meta_boxes' ) );
				add_action('manage_edit-' . $post_type_name . '_columns', array( $this, 'add_offsite_link_to_columns') );
				add_action('manage_' . $post_type_name . '_posts_custom_column', array( $this, 'add_offsite_link_column_data') );
			}
			add_filter('posts_clauses', array($this, 'maybe_modify_admin_queries'), 10, 2);
			add_action('save_post', array( $this, 'save_meta_boxes' ), 10, 2);
		}
	}

	public function setup_meta_boxes( \WP_Post $post ) {
		add_meta_box(
			$this->_prefix . 'offsite_link_box',
			__('Offsite Redirect URL', $this->_prefix . 'language'),
			array( $this, 'draw_offsite_link_box' ),
			$post->post_type,
			'advanced',
			'default'
		);
	}

	public function draw_offsite_link_box( \WP_Post $post ) {
		wp_nonce_field("{$this->_prefix}updating_offsite_link", "{$this->_prefix}offsite_link_nonce");

		$current_url = get_post_meta( $post->ID, "_{$this->_prefix}offsite_url", true );
		echo <<<HTML
<p>
	<label for="{$this->_prefix}offsite_url">Offsite Link URL:</label>
	<input type="text" name="{$this->_prefix}offsite_url" id="{$this->_prefix}offsite_url" value="{$current_url}" />
</p>
<p class="description">If set, links from your blog and archive pages will redirect to this url instead of displaying this item on your own site.</p>
HTML;

	}

	# prepend offsite link before the date
	public function add_offsite_link_to_columns( $columns ) {
		$revised = array();
		foreach ( $columns as $id => $data ) {
			if ( 'date' == "{$id}" ) {
				$revised["{$this->_prefix}offsite_link"] = __('Offsite Link' );
				$revised["{$id}"] = $data;
			} else {
				$revised["{$id}"] = $data;
			}
		}
		return $revised;
	}

	# second parameter (not used) is post_id
	public function add_offsite_link_column_data( $column ) {
		switch ( $column ) {
			case "{$this->_prefix}offsite_link":
				global $post;
				$key_name = "{$this->_prefix}offsite_link";
				if ( isset( $post->{$key_name} ) && !empty( $post->{$key_name} ) ) {
					$offsite_url = $post->{$key_name};
					echo <<<HTML
<a href="{$offsite_url}">link</a>
HTML;

				} else {
					echo '---';
				}
				break;
			default:
				break;
		}
	}

	public function maybe_modify_admin_queries( $pieces, \WP_Query $wp_query ) {
		$prefix = $this->_prefix;
		if ( $wp_query->is_admin ) {
			if ( $wp_query->is_main_query() ) {
				if ( is_array($wp_query->query) && array_key_exists('post_type', $wp_query->query) ) {
					$default_types = array();
					$allowed_post_types = $this->get_option('use_post_types', $default_types);
					if ( in_array($wp_query->query['post_type'], $allowed_post_types) ) {
						global $wpdb;
						$url_key = "_{$prefix}offsite_url";
						if ( false === strpos( $pieces['fields'], "{$prefix}os_meta" ) ) {
							$pieces['join'] .= "LEFT JOIN {$wpdb->prefix}postmeta as {$prefix}os_meta ON  {$wpdb->prefix}posts.ID = {$prefix}os_meta.post_id AND {$prefix}os_meta.meta_key = '{$url_key}' ";

							$pieces['fields'] .= ", IFNULL({$prefix}os_meta.meta_value, '') as {$prefix}offsite_url";
						}
					}
				}
			}
		}
		return $pieces;
	}

	public function save_meta_boxes( $post_id, \WP_Post $post = null ) {

		$do_save = true;
		$failure = '';
		$default_post_types = array();
		$allowed_post_types = $this->get_option('use_post_types', $default_post_types);
		if ( empty($post_id) || empty( $post ) || $this->_saved_meta_boxes ) {
			$do_save = false;
		} else if ( ! in_array( $post->post_type, $allowed_post_types ) ) {
			$do_save = false;
		}

		# Do not save for revisions or autosaves
		if ( defined('DOING_AUTOSAVE') && is_int( wp_is_post_revision( $post ) ) || is_int( wp_is_post_autosave( $post ) ) ) {
			$do_save = false;
		}

		# Make sure proper post is being worked on
		if ( !array_key_exists('post_ID', $_POST) || $post_id != $_POST['post_ID'] ) {
			$do_save = false;
		}

		# Make sure we have the needed permissions to save
		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			$do_save = false;
		}

		$nonce_field_name = $this->_prefix . 'offsite_link_nonce';
		$nonce_expected_key = $this->_prefix . 'updating_offsite_link';
		if ( ! array_key_exists( $nonce_field_name, $_POST) ) {
			$do_save = false;
		} else if ( ! wp_verify_nonce( $_POST["{$nonce_field_name}"], "{$nonce_expected_key}" ) ) {
			$do_save = false;
		} else if ( ! check_admin_referer( $nonce_expected_key, $nonce_field_name ) ) {
			$do_save = false;
		}
		if ( $do_save ) {
			$field_id = $this->_prefix . 'offsite_url';
			$offsite_url = ( array_key_exists( $field_id, $_POST ) )? stripcslashes( $_POST["{$field_id}"] ) : '';
			$current_url = get_post_meta( $post_id, "_{$this->_prefix}offsite_url", true);
			if ( $offsite_url !== $current_url ) {
				if ( empty( $offsite_url) ) {
					delete_post_meta( $post_id, "_{$this->_prefix}offsite_url" );
				} else {
					update_post_meta( $post_id, "_{$this->_prefix}offsite_url", $offsite_url );
				}
			}
			$this->_saved_meta_boxes = true;
		}
		return;
	}


	public function setup_plugin_settings() {
		register_setting(
			"reading",
			"{$this->_options_key}",
			array( $this, 'validate_options' )
		);

		add_settings_field(
			"{$this->_prefix}use_post_types",
			"Post Types for Offsite Links",
			array( $this, 'display_use_post_types'),
			"reading",
			"default"
		);

		add_settings_field(
			"{$this->_prefix}referrer",
			"Steemit Offsite Referrer Name",
			array( $this, 'display_referrer'),
			"reading",
			"default"
		);

	}

	public function display_referrer() {
		$current = $this->get_option('referrer', '');
		echo <<<HTML
<input type="text" name="{$this->_options_key}[referrer]" value="{$current}" />
HTML;

	}

	public function display_use_post_types() {
		$current_types = $this->get_option('use_post_types', array());

		$allowed = $this->get_available_post_types();
		if ( 0 === count($allowed) ) {
			echo <<<HTML
<p>There are currently no available public post types with editor capabilities to choose from.</p>
<p>If you want to allow post types without editor capabilities, use the {$this->_prefix}require_editor filter and return false.</p>
HTML;

		} else {
			$items = array();
			foreach ( $allowed as $post_type_name => $display_name ) {
				$checked = in_array( $post_type_name, $current_types )? ' checked="checked"' : '';
				$items[] = <<<HTML
<input type="checkbox" name="{$this->_options_key}[use_post_types][]" value="{$post_type_name}" {$checked} /> {$display_name}
HTML;

			}
			$item_list = implode('<br />', $items);
			echo <<<HTML
<p>{$item_list}</p>
<p>Post types chosen above will have an Offsite URL meta box added to their editor so that you can enter urls from other sites and have your posts link to them when shown on your blog or archive pages.</p>
HTML;

		}

	}

	public function validate_options( $input ) {
		$allowed = $this->get_available_post_types();
		$current_options = $this->get_options();
		if ( array_key_exists('use_post_types', $input) ) {
			$chosen = array();
			if ( is_array( $input['use_post_types'] ) && 0 < count($input['use_post_types'] ) ) {
				foreach ( $input['use_post_types'] as $selection ) {
					$selection = sanitize_text_field( $selection );
					if ( array_key_exists( $selection, $allowed ) ) {
						$chosen[] = $selection;
					} else {
						add_settings_error(
							"{$this->_prefix}use_post_types",
							"{$this->_prefix}notfounderror",
							"Unknown post type {$selection}. Please fix.",
							'error'
						);
					}
				}
			}
			$current_options['use_post_types'] = $chosen;
		}
		if ( array_key_exists('referrer', $input) ) {
			$current_options['referrer'] = sanitize_text_field( $input['referrer'] );
		}
		return $current_options;
	}
}