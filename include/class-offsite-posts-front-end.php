<?php
namespace Ninja\Privateer\Wp\Plugins\OffsitePosts;

class Front_End extends Offsite_Posts_Core {

	protected $_available_post_types = null;

	public function __construct(
		$plugin_path = '',
		$plugin_url = '',
		$plugin_prefix = 'pws_osp_',
		$options_key = 'pws_osp_plugin_options'
	) {
		parent::__construct( $plugin_path, $plugin_url, $plugin_prefix, $options_key );
	}

	protected function do_initialization() {
		parent::do_initialization();

		add_action( 'template_redirect', array( $this, 'maybe_redirect_offsite' ) );
		# add_filter( 'get_comments_link', array( $this, 'maybe_filter_comments_link' ), 20, 2 );
		# add_filter( 'post_link', array( $this, 'maybe_filter_post_link' ), 20, 2 );

	}

	public function maybe_filter_post_link( $url, \WP_Post $post = null ) {
		if ( ! is_admin() ) {
			if ( !is_null( $post ) ) {
				$post_type = $post->post_type;
				$possible_types = $this->get_available_post_types();
				if ( array_key_exists( $post_type, $possible_types ) ) {
					$offsite_link = get_post_meta( $post->ID, "_{$this->_prefix}offsite_url", true);
					if ( !empty( $offsite_link ) ) {
						$referrer = $this->get_option('referrer', '');
						if ( !empty( $referrer ) ) {
							$offsite_link .= '?r=' . urlencode( $referrer );
						}
						$url = $offsite_link;
					}
				}
			}
		}
		return $url;
	}

	public function maybe_redirect_offsite() {
		if ( is_singular() ) {
			$possible_post_types = $this->get_option('use_post_types', array());
			if ( is_singular( $possible_post_types ) ) {
				global $wp_query;
				$item_id = $wp_query->get_queried_object_id();
				$offsite_link = get_post_meta( $item_id, "_{$this->_prefix}offsite_url", true);
				if ( !empty( $offsite_link ) ) {
					$referrer = $this->get_option('referrer', '');
					if ( !empty( $referrer ) ) {
						$offsite_link .= '?r=' . urlencode( $referrer );
					}
					wp_redirect( $offsite_link, 302 );
					exit();
				}
			}
		}
	}

	public function maybe_filter_comments_link( $link, $post_id ) {
		$post_ob = get_post( $post_id );
		if ( is_a( $post_ob, '\WP_Post' ) ) {
			$post_type = $post_ob->post_type;
			$possible_types = $this->get_available_post_types();
			if ( array_key_exists( $post_type, $possible_types ) ) {
				$offsite_link = get_post_meta( $post_ob->ID, "_{$this->_prefix}offsite_url", true);
				if ( !empty( $offsite_link ) ) {
					$referrer = $this->get_option('referrer', '');
					if ( !empty( $referrer ) ) {
						$link = $offsite_link . '?r=' . urlencode( $referrer ) . '#comments';
					} else {
						$link = $offsite_link . '#comments';
					}
				}
			}
		}
		return $link;
	}
}
